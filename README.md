# themes-child

Tutti i temi di Wordpress supportano un meccanismo di "temi derivati", in cui alcuni file possono venire cambiati generando un tema -child e attivando alcune modifiche (es: rimpiazzare solo footer.php) sopra al tema padre.

Questo ci permette di fare delle modifiche ai temi senza dover creare delle patch.

Non mi è ancora chiaro se è una strategia realmente più semplice di quella di patchare i temi, ma per provare a fare un confronto ho creato questo repository dove mantenere tutte le versioni ```-child``` dei temi che usiamo.
